import json
import requests
import os

# vars
trello_api_key = os.environ.get("TRELLO_API_KEY")
trello_api_token = os.environ.get("TRELLO_API_TOKEN")
trello_board_id = os.environ.get("TRELLO_BOARD_ID")
invoice_ninja_api_token = os.environ.get("INVOICE_NINJA_API_TOKEN")
invoice_ninja_client_id = os.environ.get("INVOICE_NINJA_CLIENT_ID")
# trello_list_invoice_ninja_status_map = json.loads(os.environ.get("TRELLO_LIST_INVOICE_NINJA_STATUS_MAP"))

trello_list_invoice_ninja_status_map = {
    "backlog": {
        "trello_list": "6022ea4a1522bf809bb71ccc",
        "invoice_ninja_status": "1"
        },
    "blocked": {
        "trello_list": "5f352a60eb9ce827d7ef79f7",
        "invoice_ninja_status": "5"
        },
    "todo": {
        "trello_list": "5f3529e4886f6e16019c7649",
        "invoice_ninja_status": "2"
        },
    "doing": {
        "trello_list": "5f3529e4886f6e16019c7647",
        "invoice_ninja_status": "3"
        },
    "done": {
        "trello_list": "5f3529e4886f6e16019c764c",
        "invoice_ninja_status": "4"
        }
    }

trello_base_api = "https://api.trello.com/1"
trello_auth_string = "?key=" + trello_api_key + "&token=" + trello_api_token
invoice_ninja_tasks_url = "https://app.invoiceninja.com/api/v1/tasks"

app_json_headers = {
    "Accept": "application/json"
    }
invoice_ninja_base_headers = {
    **app_json_headers,
    "X-Ninja-Token": invoice_ninja_api_token,
    "X-Requested-With": "XMLHttpRequest"
    }
invoice_ninja_put_headers = { **invoice_ninja_base_headers, "Content-Type": "application/json"}


def read_board_cards():
    board_cards = json.loads(requests.get(url=get_trello_board_api_url(trello_board_id)).text)
    return board_cards

def get_trello_board_api_url(trello_board_id):
    return trello_base_api + "/boards/" + trello_board_id + "/cards" + trello_auth_string

def get_trello_card_api_url(trello_task):
    return trello_base_api + "/cards/" + trello_task['id'] + trello_auth_string

def read_invoice_ninja_tasks():
    invoice_ninja_tasks_in_page = json.loads(requests.get(url=(invoice_ninja_tasks_url + "?client_id=" + invoice_ninja_client_id), headers=invoice_ninja_base_headers).text)
    invoice_ninja_all_active_tasks = []
    while True: # do while loop
        for task in invoice_ninja_tasks_in_page['data']:
            if task['archived_at'] == 0:
                invoice_ninja_all_active_tasks.append(task)
        if not 'next' in invoice_ninja_tasks_in_page['meta']['pagination']['links'].keys():
            break
        invoice_ninja_tasks_in_page = json.loads(requests.get(url=(invoice_ninja_tasks_in_page['meta']['pagination']['links']['next'] + "&client_id=" + invoice_ninja_client_id), headers=invoice_ninja_base_headers).text)
    return invoice_ninja_all_active_tasks

def sync_invoice_ninja_tasks(trello_tasks, invoice_ninja_tasks):
    for invoice_ninja_task in invoice_ninja_tasks:
        if not invoice_ninja_task_exists_in_trello(trello_tasks, invoice_ninja_task):
            delete_invoice_ninja_task(invoice_ninja_task)
    for trello_task in trello_tasks:
        if not trello_task_exists_in_invoice_ninja(trello_task, invoice_ninja_tasks):
            create_invoice_ninja_task(trello_task)
        else:
            for invoice_ninja_task in invoice_ninja_tasks:
                if tasks_match_bool(trello_task, invoice_ninja_task):
                    update_invoice_ninja_task(trello_task, invoice_ninja_task)
                    continue

def invoice_ninja_task_exists_in_trello(trello_tasks, invoice_ninja_task):
    found = False
    for trello_task in trello_tasks:
        if tasks_match_bool(trello_task, invoice_ninja_task):
            found = True
    return found

def trello_task_exists_in_invoice_ninja(trello_task, invoice_ninja_tasks):
    found = False
    for invoice_ninja_task in invoice_ninja_tasks:
        if tasks_match_bool(trello_task, invoice_ninja_task):
            found = True
    return found

def tasks_match_bool(trello_task, invoice_ninja_task):
    if trello_task['id'] == invoice_ninja_task['custom_value1']:
        return True
    else:
        return False

def create_invoice_ninja_task(trello_task):
    input_data = get_invoice_ninja_input_data(trello_task)
    new_invoice_ninja_task = json.loads(requests.post(url=invoice_ninja_tasks_url, headers=invoice_ninja_put_headers, json=input_data).text)
    return new_invoice_ninja_task

def update_invoice_ninja_task(trello_task, invoice_ninja_task):
    if tasks_match_bool(trello_task, invoice_ninja_task):
        input_data = get_invoice_ninja_input_data(trello_task)
        updated_invoice_ninja_task = json.loads(requests.put(url=invoice_ninja_tasks_url + '/' + str(invoice_ninja_task['id']), headers=invoice_ninja_put_headers, json=input_data).text)
    return updated_invoice_ninja_task

def get_invoice_ninja_input_data(trello_task):
    input_data = {
        "client_id": int(invoice_ninja_client_id),
        "description": trello_task['name'] + "\n" + trello_task['desc'],
        "custom_value1": trello_task['id'],
        "task_status_id": get_matching_ninja_status_for_trello_task(trello_task)
        }
    return input_data

def delete_invoice_ninja_task(invoice_ninja_task):
    requests.put(url=(invoice_ninja_tasks_url + "/" + str(invoice_ninja_task['id']) + "?action=delete"), headers=invoice_ninja_base_headers)

def get_matching_ninja_status_for_trello_task(trello_task):
    for key in trello_list_invoice_ninja_status_map:
        if trello_task['idList'] == trello_list_invoice_ninja_status_map[key]['trello_list']:
            return trello_list_invoice_ninja_status_map[key]['invoice_ninja_status']

def archive_invoiced_invoice_ninja_tasks(invoice_ninja_tasks):
    invoiced_tasks = []
    for invoice_ninja_task in invoice_ninja_tasks:
        if invoice_ninja_task['invoice_id'] != 0:
            invoiced_tasks.append(json.loads(requests.put(url=invoice_ninja_tasks_url + '/' + str(invoice_ninja_task['id']) + "?action=archive", headers=invoice_ninja_put_headers).text)['data'])
    return invoiced_tasks

def remove_invoiced_tasks_from_trello(trello_tasks, invoiced_invoice_ninja_tasks):
    for invoice_ninja_task in invoiced_invoice_ninja_tasks:
        for trello_task in trello_tasks:
            if tasks_match_bool(trello_task, invoice_ninja_task):
                delete_trello_task(trello_task)

def delete_trello_task(trello_task):
    requests.delete(url=get_trello_card_api_url(trello_task), headers=app_json_headers)

def main():
    trello_tasks = read_board_cards()
    invoice_ninja_tasks = read_invoice_ninja_tasks()
    sync_invoice_ninja_tasks(trello_tasks, invoice_ninja_tasks)
    invoiced_invoice_ninja_tasks = archive_invoiced_invoice_ninja_tasks(invoice_ninja_tasks)
    remove_invoiced_tasks_from_trello(trello_tasks, invoiced_invoice_ninja_tasks)

if __name__ == "__main__":
    main()

def handler(event, context):
    main()