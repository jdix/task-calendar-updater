#!/bin/bash
set -ex

rm -rf setup .venv package.zip
python3 -m venv .venv
source .venv/bin/activate
mkdir setup

rsync -r ./* ./setup/ --exclude .gitignore --exclude README.md --exclude .venv --exclude .vscode --exclude setup --exclude requirements.txt --exclude package_python_for_lambda.sh

cd setup
python3 -m pip install -r ../requirements.txt -t .
zip -r ../package.zip ./*
cd ..
rm -rf ./setup
deactivate
rm -rf ./.venv

# export PIPENV_VENV_IN_PROJECT=1

# function parse_git_ignore_for_excludes {
    
# }

# exclude_str
# excludes
